# README #

Описание стратегии "Парного трейдинга" доступно в статье: [Парный трейдинг: описание стратегии на Python](https://quantrum.me/875-parnyj-trejding-opisanie-strategii-na-python/).

Подробное описание доступно в статье: [Парный трейдинг: использование МНК для расчета размеров позиций](https://quantrum.me/1345-parnyj-trejding-ispolzovanie-mnk-dlya-rascheta-razmerov-pozicij/).

В файле quantopian.py доступен алгоритм Quantopian.

## Database fields (PostgreSQL) ##

* **symbol**	character varying(6)
* **dt**	date
* **open**	bigint [0]
* **high**	bigint [0]
* **low**	bigint [0]
* **close**	bigint [0]
* **volume**	numeric(20,0) [0]
* **adj**	numeric(20,0) NULL

