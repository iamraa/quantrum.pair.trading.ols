import psycopg2
import psycopg2.extras
from datetime import datetime, date, timedelta
from pandas_datareader import data, wb
import numpy as np

class Db(object):
    _connection = None

    def __init__(self, host="localhost", user="user", password="1", db="db"):
        try:
            self._connection = psycopg2.connect("host='{0}' user='{1}' password='{2}' dbname='{3}'".format(
                    host, user, password, db))
        except psycopg2.Error as err:
            print("Connection error: {}".format(err))
            self._connection.close()

    def query(self, sql, params=None, cursor='list'):
        if not self._connection:
            return False

        data = False

        if cursor == 'dict':
            # Assoc cursor
            factory = psycopg2.extras.DictCursor
        else:
            # Standard cursor
            factory = psycopg2.extensions.cursor
        try:
            cur = self._connection.cursor(cursor_factory=factory) # by column name
            cur.execute(sql, params)
            data = cur.fetchall()
        except psycopg2.Error as err:
            print("Query error: {}".format(err))

        return data

db = Db(host="localhost", user="developer", password="1", db="go_finance")

table = 'prices'
order_dt = 'ASC'
def prices(symbols, dt_from=date.today(), period=90, is_adj=True):
    """
    Get prices from database for one or multiple symbols.
    """
    dt_to = dt_from - timedelta(days=period)
    cond = {
        'symbols': symbols,
        'to': dt_from,
        'from': dt_to
    }

    # FIXME Change to view
    sql = """
        SELECT
            symbol,
            dt,
            ROUND(CASE WHEN adj IS NOT NULL AND close > 0
                THEN adj / close::real ELSE 1 END * open) / {0} as open,
            ROUND(CASE WHEN adj IS NOT NULL AND close > 0
                THEN adj / close::real ELSE 1 END * high) / {0} as high,
            ROUND(CASE WHEN adj IS NOT NULL AND close > 0
                THEN adj / close::real ELSE 1 END * low) / {0} as low,
            CASE WHEN adj IS NOT NULL THEN adj ELSE close
                END::real / {0} as close,
            volume::int
        FROM {1}
        WHERE symbol IN (SELECT unnest(%(symbols)s)) AND dt BETWEEN %(from)s AND %(to)s
        ORDER BY dt {2}""".format(10000, table, order_dt)
    
    if not is_adj:
        sql = """
        SELECT
            symbol,
            dt,
            open::real / {0} as open,
            high::real / {0} as high,
            low::real / {0} as low,
            close::real / {0} as close,
            volume::int
        FROM {1}
        WHERE symbol IN (SELECT unnest(%(symbols)s)) AND dt BETWEEN %(from)s AND %(to)s
        ORDER BY dt {2}""".format(10000, table, order_dt)

    return db.query(sql, cond)


def get_performance(a):
    """
    Convert vector to performance
    """
    a = a.round(2)
    return np.insert(np.cumsum(np.diff(a) / a[:-1] * 100.), 0, 0)


def prepare_pair(S1, S2, to_performance=True):
    """
    Cut vectors to the minimum length
    """
    # проверяем длину истории и обрезаем до минимальной
    if len(S1) < len(S2):
        S2 = S2[-len(S1):]
    elif len(S2) < len(S1):
        S1 = S1[-len(S2):]

    # конвертируем в относительные величины
    if to_performance:
        S1 = get_performance(S1)
        S2 = get_performance(S2)

    return S1, S2

def get_prices_yahoo(symbols, dt_end=date.today()):
    stocks = symbols
    ls_key = 'Adj Close'
    start = dt_end - timedelta(days=700)
    end = dt_end
    f = data.DataReader(stocks, 'yahoo', start, end)
    return f['Adj Close']

def months_generator(x):
    iterator = iter(x)
    prev_item = None
    for item in iterator:
        if prev_item is None or prev_item.month != item.month:
            yield item
        prev_item = item