"""
Парный трейдинг. Дневной таймфрейм.
Использования МНК для размеров позиций и z-оценки.

Пример из комментариев:
https://www.quantopian.com/posts/how-to-build-a-pairs-trading-strategy-on-quantopian

# 2015: ('DRE', 'O'); ('CIT', 'STT'); ('ALNY', 'DATA'); ('AWK', 'DAL'); PNR, FDX; ('BHI', 'HLT'); ('MMP', 'PH')

"""
import talib
import pandas as pd
import numpy as np
import statsmodels.api as sm
from statsmodels.tsa.stattools import adfuller


# подготовка теста
def initialize(context):
    # пара
    context.pair = tuple(
        symbols('DRE', 'O'),
    )
    context.opened = dict()
    context.opened[context.pair] = 'closed'

    # OLS
    context.hedgeRatioTS = np.array([])
    context.HRlag = 2
    context.spread = np.array([])
    context.lookback = 20
    context.z_window = 20

    schedule_function(trade_zscore, date_rules.every_day(), time_rules.market_open(hours=1))

# ежедневная проверка сигналов
def trade_zscore(context, data):
    stocks = context.pair

    can_trade = data.can_trade(stocks)

    # выходим, если не можем торговать одной из акций
    for stock in stocks:
        if not can_trade[stock]:
            log.warn("Can't trade " + stock.symbol)
            return

    # загрузка исторических данных
    hist = data.history(stocks, ['open', 'high', 'low', 'close'], 500, '1d')

    # переводим в относительные значения
    check_length = 120
    performance = [
        get_performance(hist['close'][stocks[0]][-check_length:].as_matrix()),
        get_performance(hist['close'][stocks[1]][-check_length:].as_matrix())
    ]

    spread_performance = pd.Series(performance[0] - performance[1])
    spread_price = hist['close'][stocks[0]][-check_length:] - hist['close'][stocks[1]][-check_length:]

    spread = spread_price
    spread = spread_performance
    zscore = zscore_std(spread)  # standard
    #zscore = z_score_ma(spread, short=5, long=50)  # by MA cross

    # сигнал
    z = zscore.iloc[-1]

    if len(spread[-check_length:].dropna()) < check_length:
        log.warn("Can't trade with short length")
        return

    # проверка стационарности
    result = adfuller(spread.iloc[-check_length:])
    score, pvalue, crit = result[0], result[1], result[4]
    coint = score < crit['5%']
    coint = True  # проверка отключена

    # используем OLS для расчет размера позиций
    context.lookback = 20
    Y = hist['close'][stocks[0]][-context.lookback:]
    X = hist['close'][stocks[1]][-context.lookback:]
    try:
        hedge = hedge_ratio(Y, X, add_const=True)
    except ValueError as e:
        log.debug(e)
        return

    context.hedgeRatioTS = np.append(context.hedgeRatioTS, hedge)
    # рассчитываем сегодняшнюю величину спреда и добавляем в историю
    if context.hedgeRatioTS.size < context.HRlag:
        return
    # получаем величину отношения размера позиции за прошлый день
    hedge = context.hedgeRatioTS[-context.HRlag]
    context.spread = np.append(context.spread, Y[-1] - hedge * X[-1])

    # z-оценка на основе истории величины спреда
    z_hedge = 0
    if context.spread.size > context.z_window:
        # оставляем ограниченную длину истории величины спреда
        spreads = context.spread[-context.z_window:]
        z_hedge = (spreads[-1] - spreads.mean()) / spreads.std()

    z = z_hedge

    if not coint:
        # если пропала стационарность, закрываемся
        order_target_percent(stocks[0], 0)
        order_target_percent(stocks[1], 0)
        context.opened[stocks] = 'closed'
    elif z > 1 and context.opened[stocks] == 'closed':
        # если опережает акция А
        order_target_percent(stocks[0], -1)
        order_target_percent(stocks[1], 1*(hedge))
        context.opened[stocks] = 'short'
    elif z < -1 and context.opened[stocks] == 'closed':
        # если опережает акция Б
        order_target_percent(stocks[0], 1)
        order_target_percent(stocks[1], -1*(hedge))
        context.opened[stocks] = 'long'
    elif (context.opened[stocks] == 'long' and z > 0) or (context.opened[stocks] == 'short' and z < 0):
        # рядом с нулем закрываем позицию
        order_target_percent(stocks[0], 0)
        order_target_percent(stocks[1], 0)
        context.opened[stocks] = 'closed'

    # собираем историю значений
    sign = abs(z) / z if z else 0
    value0 = context.portfolio.positions[stocks[0]].amount * context.portfolio.positions[stocks[0]].last_sale_price
    value1 = context.portfolio.positions[stocks[1]].amount * context.portfolio.positions[stocks[1]].last_sale_price
    record(**{
            'zscore': z if abs(z) <= 2.05 else sign*2.05,
            'z-perf': zscore_std(spread_performance).iloc[-1],
            #'z-price': zscore_std(spread_price).iloc[-1],
            #'z-hedge': z_hedge,
            stocks[0].symbol: value0,
            stocks[1].symbol: value1
          })

def zscore_std(series):
    return (series - series.mean()) / np.std(series)

def get_performance(a):
    """
    Convert vector to performance
    """
    a = a.round(2)
    return np.insert(np.cumsum(np.diff(a) / a[:-1] * 100.), 0, 0)

def get_diff(y):
    # Метод наименьших квадратов
    x = np.array([i for i in range(len(y))])
    arr = np.vstack([x, np.ones(len(x))]).T
    m, c = np.linalg.lstsq(arr, y)[0]
    #print(m, c)
    return c

def hedge_ratio(Y, X, add_const=True):
    if add_const:
        X = sm.add_constant(X)
        model = sm.OLS(Y, X).fit()
        return model.params[1]
    model = sm.OLS(Y, X).fit()
    return model.params.values